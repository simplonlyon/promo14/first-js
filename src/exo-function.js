
/**
 * 
 * @param {number} paramNumber 
 * @returns {boolean}
 */
function isOdd(paramNumber) {
    if(paramNumber % 2 === 0) {
        return false;
    } 
    return true;
    
}

//ça fait pareil, vu que techniquement l'opérateur de comparaison
//renvoie déjà true ou false, on peut juste directement le return
// function isOdd(paramNumber) {
//     return paramNumber % 2 !== 0;
// }

//pareil, mais en fat-arrow function
// const isOdd = (paramNumber) => paramNumber % 2 !== 0;

console.log(isOdd(6));
console.log(isOdd(3));
console.log(isOdd(4));


/**
 * Fonction qui cherche un truc dans un tableau
 * @param {any} valueToSearch Le truc qu'on cherche
 * @param {any[]} array Le tableau dans lequel on cherche le truc
 * @returns {boolean}
 */
function arrayContains(valueToSearch, array) {
    for (let item of array) {
        if (item === valueToSearch) {
            return true;
        }
    }
    return false;
}

let tableau = ['bloup', 'blip', 'blop'];

console.log(arrayContains('dfmqjsdlfkj', tableau));

/**
 * Fonction qui fait la somme de tous les trucs dans le tableau
 * @param {number[]} numberArray 
 * @returns {number} la somme de truc
 */
function sum(numberArray) {

    let total = 0;
    for (const item of numberArray) {
        total+=item;
    }
    return total;
}


console.log(sum([2,3,4]));

/**
 * 
 * @param {number[]} arrayNumber 
 * @returns {boolean}
 */
function isSumOdd(arrayNumber) {
    let total = sum(arrayNumber);
    return isOdd(total);
}

console.log(isSumOdd([1,2,3]));


/**
 * 
 * @param {array} array 
 * @returns {number}
 */
function recursiveSum(array) {

    let total = 0;
    for (const item of array) {
        if(Array.isArray(item)) {
            total += recursiveSum(item);
        } else {
            total+=item;

        }
    }
    return total;

}

