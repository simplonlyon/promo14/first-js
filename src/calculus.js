
// let min = 1;
// let max = 10;
// let a = Math.floor(Math.random()*(max-min))+min;
// let b = Math.floor(Math.random()*(max-min))+min;

//On utilise Number(...) pour faire en sorte de convertir le résultat
//du prompt (qui par défaut est du string) en number
let a = Number(prompt('What is number 1 ?'));
let b = Number(prompt('What is number 2 ?'));

//On récupère l'opérateur mathématique à utiliser
let operator = prompt('Which operator do you want to use ?');

console.log(operator);

//On vérifie si a et b sont bien des numbers. 
if (!isNaN(a) && !isNaN(b)) {



    if (operator === '+') {
        console.log(a + b);
    }
    if (operator === '-') {
        console.log(a - b);
    }
    if (operator === '*') {
        console.log(a * b);
    }
    if (operator === '/') {
        console.log(a / b);
    }


    //ce switch fait exactement la même chose que les if-elseif-else
    //en dessous
    /*switch (operator) {
        case '+':
            console.log(a + b);
            break;
        case '-':
            console.log(a - b);
            break;
        case '*':
            console.log(a * b);
            break;
        case '/':
            console.log(a / b);
            break;
        default:
            console.log('Invalid operator')
            break;
    }*/

    /*
    if(operator === '+') {
        console.log(a+b);
    } else if(operator === '-') {
        console.log(a-b);
    } else if(operator === '*') {
        console.log(a*b);
    } else if(operator === '/') {
        console.log(a/b);
    } else {
        console.log('Invalid operator');
    }
    */
}
