//affiche un prompt dans le navigateur et stock la 
//réponse du user dans la variable auquel on l'assigne
//let reponse = prompt('Salut ça va ?');


//Faire un prompt qui demande le prénom du user
//et si le prénom donné est le votre, 
//afficher 'you rock'. Dans tous les cas, afficher
//'Hello' suivi du prénom donné (tout ça en console)
let answer = prompt('Who are you ?');

console.log('Hello '+answer);
if(answer === 'promo 14') {
    console.log('You rock');
}

//Les opérateur logique permettent d'enchaîner des conditions
//dans le cas où on veut vérifier si une chose et vraie ET(&&)/OU(||) 
//une autre aussi
let condition1 = true && true; //true
let condition2 = true && false; // false
let condition3 = true || true; // true
let condition4 = true || false; // true
let condition5 = false || false; // false

