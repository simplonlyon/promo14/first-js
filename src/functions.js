
/**
 * Fonction qui fait une addition
 * @param {number} a le premier nombre
 * @param {number} b le deuxième nombre
 * @returns {number} le résultat de l'addition
 */
function addition(a, b) {
    return a+b;

}
//ça fait pareil qu'au dessus (fat arrow function)
// const addition = (a,b) => {
    
//     return a+b;
// };

//ça aussi, le return est fait par défaut
// const addition = (a,b) => a+b;



let result = addition(1,2);
console.log(result);

addition(2,5);
addition(6,4)

addition(1, 3)

/**
 * Fonction qui dit bonjour
 * @param {string} firstName 
 * @returns {string}
 */
function greeting(firstName) {
    return 'Hello '+firstName;
}

let firstName = greeting();


console.log(greeting(firstName));