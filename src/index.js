let maPremiereVariable = 10;

//Faire une variable avec votre prénom dedans
let prenom = 'Jean';

console.log('coucou');

//Afficher en console le contenu de la première variable
//puis afficher le contenu de prenom
console.log(maPremiereVariable);
console.log(prenom);

let stringContent = 'Jean';
let numberContent = 10;
let booleanContent = true; //false
let undefinedContent = undefined;
let nullContent = null;

/*Créer deux variables a et b et mettre dedans des 
valeurs numériques, puis faire en sorte d'afficher
dans la console le résultat de l'addition de ces deux
valeurs*/
let a = 5;
let b = 2;
console.log(a+b);

//Créer une troisième variable result et mettre le 
//résultat de la multiplication de ces deux valeurs
//dedans, puis afficher le résultat en console
let result = a*b;
console.log(result);

//Créer deux variables avec du texte dedans, et mettre
//dans une troisième variable le résultat de l'addition
//de ces deux textes, puis l'afficher en console
let firstText = 'Simplon';
let secondText = 'Lyon';
let fullText = firstText+secondText;
console.log(fullText);

console.log(result+fullText);

//Créer une variable avec 1 dedans, puis changer sa valeur pour 
//l'additionner avec elle même
let incremental = 1;
incremental = incremental+incremental;
incremental += incremental;
console.log(incremental);

fullText+=firstText;
fullText+=firstText;

fullText+=fullText;
console.log(fullText);

let exampleNumber = 10;
//Ces trois trucs, font la même chose : ajoute 1 à la valeur de la variable
exampleNumber = exampleNumber+1;
exampleNumber += 1;
exampleNumber++;
exampleNumber--;
