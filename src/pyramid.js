let starNb = 1;
let spaceNb = 4;

for(let x=0; x < 5; x++) {

    let stars = '';
    for(let y = 0; y < starNb;y++) {
        stars += '*';

    }
    let spaces = '';
    for(let z = 0; z < spaceNb; z++) {
        spaces += ' ';
    }
    console.log(spaces+stars);

    starNb += 2;
    spaceNb -= 1;
}

/*
let stars = '*';

for (let y = 4; y >= 0; y--) {
    let spaces = '';

    for (let x = 0; x < y; x++) {

        spaces+=' ';

    }

    console.log(spaces+stars);
    stars += '**';
}

*/

/*
let starNb = 1;
let spaceNb = 4;

for(let x=0; x < 5; x++) {

    console.log(' '.repeat(spaceNb)+'*'.repeat(starNb));

    starNb += 2;
    spaceNb -= 1;
}
*/