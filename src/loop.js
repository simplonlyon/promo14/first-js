//Ici, on fait une boucle dont le nombre de tour est déterminé par 
//un prompt, et dont le message à afficher est également déterminé par
//un autre prompt
// let loopNb = prompt('How many loop ?');
// let message = prompt('Which message');

// for(let x=0;x < loopNb;x++) {

//     console.log(message + x);
// }


/*
//Boucle de base 
//on indique la valeur de départ, ici 0, on indique ensuite la condition
//d'arrêt de la boucle, ici tant que x est inférieur à 10, puis on incrémente
//x à chaque tour de boucle. Ici on va donc faire 10 console log
for(let x=0; x < 10; x++) {
    console.log(x);
}
 
 */


let loopCount = prompt('how much do you love javascript?');
let sentence = 'I ❤️ Javascript';

for(let i=0; i < loopCount; i++) {
    //A chaque tour de boucle, on rajoute un morceau de phrase dans
    //notre variable sentence
    sentence += ' very ';
}

sentence += 'much';

console.log(sentence);


//Exemple de boucle imbriquée. La première boucle va faire 10 tours
for(let x = 0; x < 10; x++) {

    //Et la boucle à l'intérieur va faire 10 tour à chaque tour de 
    //la première boucle
    for(let y = 0; y < 10; y++ ){
        //donc au final on a 10*10 tours = 100 tours
    }
    //il est possible de mettre du code avant ou après la boucle imbriquée
}