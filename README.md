# Premier projet JS

## Créer et exécuter un projet JS avec snowpack
1. Dans le terminal, exécuter `npx create-snowpack-app --template @snowpack/app-template-blank nom-de-mon-projet` (en remplaçant 'nom-de-mon-projet' par le nom de votre projet). Sera créé automatiquement une structure de dossier convenable ainsi qu'un dépôt git avec un premier commit
2. Ouvrir le projet avec vscode en faisant par exemple `code nom-de-mon-projet`
3. Lancer le projet en exécutant `npm start`, cela va lancer snowpack et un serveur de développement qui se rechargera automatiquement (ne pas fermer le terminal avec lequel vous avez lancer cette commande)

Votre code javascript devra se trouver dans le dossier `src` et votre code HTML/CSS et vos images devront être dans le dossier `public` (il est tout à fait possible et conseiller de faire des sous dossiers dans ces deux dossiers)

### Pour cloner et utiliser un projet snowpack
1. Faire le `git clone`
2. Aller dans le dossier du projet avec le terminal
3. Exécuter `npm install` (ou `npm i`)
4. Faire le `npm start` et voilou


### Créer une nouvelle page dans un projet snowpack
1. Créer un fichier html dans le dossier public (example.html)
2. Créer un fichier js (example.js)
3. Dans le fichier HTML, mettre une balise script qui pointe sur `/dist/example.js` (remplacer par le nom de votre fichier)
4. Accéder à la page sur http://localhost:8080/example.html (ne pas utiliser live serveur)

## Exercices

### Calcnulatrice [js](src/calculus.js)
1. Créer un nouveau fichier html et un nouveau fichier js, qu'on va appeler calculus.html/.js
2. Créer 2 variables avec un number dedans, peu importe lesquels
3. Faire un prompt pour récupérer la valeur d'une troisième variable qui contiendra l'opérateur (+, -, *, /)
4. Faire des conditions en dessous qui vont faire un console log du résultat du calcul selon l'opérateur donné dans le prompt (genre si c'est +, alors ça fera un console log de l'addition des deux numbers)

Mini bonus intermédiaire : Faire que les 2 numbers soient choisis aléatoirement entre 1 et 10
5. Faire en sorte que les 2 numbers soient donnés par l'utilisateur·ice comme l'opérateur
6. Faire que si aucun opérateur valide n'est donné, alors on affiche un message d'erreur (donc si n'importe quelle autre valeur que +, -, *, /, alors on dit nope)


### Exo Conditions [js](src/exo-conditions.js)
1. Nouveau fichier exo-condition html/js
2. Faire 2 variables, firstName et age qu'on récupère avec 2 prompt
3. Faire une condition qui vous dira Welcome si votre prenom est Sofiane et que votre age est supérieur à 18
4. Faire une autre condition qui affichera un message si le prénom rentré contient le même nombre de lettre que l'age (exemple, si je met Jean et que j'ai 4 ans)
5. Faire encore une condition qui affichera quelque chose si l'age est entre 30 et 50 ans
6. Faire une condition qui affichera quelque chose si le prénom est Abby ou Paul mais que l'age est inférieur à 70

Bonus : Faire une condition qui vérifie que le prénom commence bien par une majuscule, qu'il fait plus de 1 caractère et que l'age est bien un number

### Exo Pyramide [js](src/pyramid.js)
Objectif final, avoir ça via des boucles : 
```
    *
   ***
  *****
 *******
*********
```
1. Créer un nouveau point d'entrée pyramid.html / pyramid.js
2. Pour commencer, on peut essayer de faire une première boucle pour avoir autant de console log qu'on a d'étage à la pyramide, donc ici 5 (peu importe ce qu'on console log)
3. Ensuite, on peut essayer de trouver un calcul très savant pour afficher à chaque tour de boucle le nombre d'étoile par étage de la pyramide, donc genre ça comme résultat :
```
1
3
5
7
9
```
4. Une fois qu'on a le bon nombre d'étage et une variable qui contient le nombre d'étoile à afficher par étage, "ya pu qu'à" faire en sorte d'afficher les étoiles en questions (par exemple avec une boucle qui concatène dans une phrase, comme à l'exo d'avant, sauf que cette boucle sera imbriquée dans l'autre)
Résultat :
```
*
***
*****
*******
*********
```
5. Ensuite on recommence un peu la même chose qu'à l'étape 3, mais cette fois ci pour le nombre d'espace avant chaque étoile pour avoir un truc qui ressemble à ça :
```
4*
3***
2*****
1*******
0*********
```
6. Enfin, on fait comme à l'étape 4 pour concaténer les espaces avant les étoiles, et on obtient notre pyramide
   
Bonus : Pouvoir changer le nombre d'étage avec une variable et pourquoi pas aussi le caractère (pour pouvoir faire une pyramide de coeurs par exemple <3)


### Exo Array + boucle
#### I. Parcourir le tableau
1. Créer un nouveau point d'entrée array.html / array.js
2. Dans le js, créer une variable promo qui va contenir un tableau (array) avec les prénoms des personnes de la promo (au moins 3-4, z'êtes pas obligé·e de mettre les 16)
3. Pour commencer, faire en sorte d'afficher en console la troisième valeur du tableau
4. Ensuite, faire en sorte d'afficher la taille du tableau (indice, c'est exactement comme pour choper le nombre de lettre dans un mot)
5. faire une boucle for classique qui va tourner tant que la valeur de x ne dépasse pas la taille du tableau, et pourquoi pas faire un console.log de x dans la boucle
6. Dans cette boucle, essayer d'utiliser x pour afficher en console chaque valeur du tableau
#### II. Faire un truc avec ce qu'on parcours
1. Faire en sorte d'afficher "Hello" suivi du nom de chaque student, pasqu'on est poli·e ici
2. Faire en sorte d'afficher "that's me" quand c'est votre prénom qui passe
3. Faire en sorte d'afficher un student sur deux (vous aurez potentiellement besoin d'une autre variable)
4. Faire en sorte d'additionner les nombre de lettres des prénoms de toutes les personnes de la promo et de l'afficher (vous pouvez commencer par afficher déjà le nombre de lettre pour chaque personne)
5. Faire en sorte d'afficher en majuscule le nom du premier et du dernier student (for...of ne sera pas votre ami pour celui ci)


## Exo Function [js](src/exo-function.js)
1. Nouveau point d'entrée exo-function.html / exo-function.js
2. Créer une fonction isOdd qui attendra en argument un number, et qui renvoie true ou false selon si le nombre est pair ou impair
3. Créer une fonction arrayContains qui va attendre 2 paramètres : une valeur quelconque, et un tableau. Cette fonction va renvoyer true ou false selon si le tableau contient la valeur ou non (à faire avec une boucle)
   
Aide :  l'idée va être de parcourir le tableau dans la fonction avec une boucle et avec une condition, vérifier si une des valeurs du tableau correspond à l'argument passé à la fonction, et si c'est le cas, on renvoie true 

4. Créer une fonction sum qui va attendre un tableau de number en argument et qui va renvoyer en number la somme de tous les éléments du tableau (ex: si j'appelle sum([1,2,3]) j'obtiendrai 6, pasque 1+2+3=6)

Aide : il va falloir créer une ptite variable initialisé à zéro dans la fonction, puis parcourir le tableau, et à chaque tour on additionne la valeur actuelle à la variable. Enfin, on fait un return de cette variable 
5. (qui a besoin du 2 et du 4) Faire une fonction isSumOdd qui va attendre un tableau de number en paramètre, puis qui va faire la somme de tous les items du tableau, et si la somme est pair, renverra true
   
Bonus: Faire une fonction sum qui marche aussi bien pour un tableau de number que pour un tableau de tableau de number, et même plus genre si je lui donne [1, [2, [3,4]]], j'aurais 10